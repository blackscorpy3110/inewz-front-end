/**
 * Created by Mr.Java on 1/27/2016.
 */
angular.module('myApp').config(function ($stateProvider, $urlRouterProvider) {

    // For any unmatched url, send to /route1
    $urlRouterProvider.otherwise("/index/home")

    $stateProvider
        .state('index', {
            url: "/index",
            abstract: true,
            views: {
                'header': {
                    templateUrl: 'dashboard/navbar-top.html',
                },

                '': {
                    template: '<div ui-view></div>'
                }
            }
        })

        .state('index.home', {
            url: '/home',
            templateUrl: "home.html",
            controller: 'viewAllPostCtrl'
        })

        .state('index.find_post_by_category', {
            params : {id : null},
            url: '/find_post_by_category',
            controller: 'findPostByCategoryCtrl',
            templateUrl: 'dashboard/controllers/post/find_post_by_category/find_post_by_category.html',
            resolve: {
                id: ['$stateParams', function($stateParams) {
                    return $stateParams.id;
                }]
            }
        })

        .state('index.find_post_by_user', {
            params : {id : null},
            url: '/find_post_by_user',
            controller: 'findPostByUserCtrl',
            templateUrl: 'dashboard/controllers/post/find_post_by_user/find_post_by_user.html',
            resolve: {
                id: ['$stateParams', function($stateParams) {
                    return $stateParams.id;
                }]
            }
        })


        .state('index.login', {
            url: '/login',
            templateUrl: 'login.html',
            controller: 'loginCtrl'
        })

        .state('index.register', {
            url: '/register',
            templateUrl: 'register.html',
            controller: 'registerCtrl'
        })

//=========== Dashboard ==================
        .state('dashboard', {
            url: '/dashboard',
            abstract: true,
            views: {
                'header': {
                    templateUrl: 'dashboard/navbar-top.html',

                },
                'menuLeft': {
                    templateUrl: 'dashboard/menu-left.html'
                },
                '': {
                    template: '<div ui-view></div>'

                }
            }
        })

        .state('dashboard.home', {
            url: "/home",
            templateUrl: 'dashboard/home.dashboard.html'
        })

        .state('dashboard.showAllUser', {
            url: "/show_all_user",
            templateUrl: 'dashboard/controllers/user/show_all_user.html',
            controller: 'userCtrl'
        })

        .state('dashboard.create_category', {
            url: '/create_category',
            templateUrl: 'dashboard/controllers/category/create_category/create_category.html',
            controller: 'createCatCtrl'
        })

        .state('dashboard.show_all_category', {
            url: '/show_all_category',
            templateUrl: 'dashboard/controllers/category/show_all_category/show_all_category.html',
            controller: 'showCatCtrl'
        })

        .state('dashboard.edit_category', {
            params : {id : null},
            url: '/edit_category',
            controller: 'editCategoryCtrl',
            templateUrl: 'dashboard/controllers/category/edit_category/edit_category.html',
            resolve: {
                id: ['$stateParams', function($stateParams) {
                    return $stateParams.id;
                }]
            }
        })

        .state('dashboard.create_post', {
            url: '/create_post',
            templateUrl: 'dashboard/controllers/post_temporary/create_post/create_post.html',
            controller: 'createPostCtrl'
        })

        .state('dashboard.show_all_post', {
            url: '/show_all_post',
            templateUrl: 'dashboard/controllers/post/show_all_post/show_all_post.html',
            controller: 'showPostCtrl'
        })

        .state('dashboard.show_all_my_post', {
            url: '/show_all_my_post',
            templateUrl: 'dashboard/controllers/post/show_all_my_post/show_all_my_post.html',
            controller: 'showMyPostCtrl'
        })

        .state('dashboard.edit_post', {
            params : {id : null},
            url: '/edit_post',
            controller: 'editPostCtrl',
            templateUrl: 'dashboard/controllers/post/edit_post/edit_post.html',
            resolve: {
                id: ['$stateParams', function($stateParams) {
                    return $stateParams.id;
                }]
            }
        })

        .state('dashboard.edit_my_post', {
            params : {id : null},
            url: '/edit_my_post',
            controller: 'editMyPostCtrl',
            templateUrl: 'dashboard/controllers/post/edit_my_post/edit_my_post.html',
            resolve: {
                id: ['$stateParams', function($stateParams) {
                    return $stateParams.id;
                }]
            }
        })

        .state('dashboard.show_all_temporary_post', {
            url: '/show_all_temporary_post',
            templateUrl: 'dashboard/controllers/post_temporary/show_all_temp_post/show_all_temporary_post.html',
            controller: 'showTempPostCtrl'
        })

        .state('dashboard.show_all_my_temporary_post', {
            url: '/show_all_my_temporary_post',
            templateUrl: 'dashboard/controllers/post_temporary/show_all_my_temp_post/show_all_my_temporary_post.html',
            controller: 'showMyTempPostCtrl'
        })

        .state('dashboard.edit_temporary_post', {
            params : {id : null},
            url: '/edit_temporary_post',
            controller: 'editTempPostCtrl',
            templateUrl: 'dashboard/controllers/post_temporary/edit_temp_post/edit_temporary_post.html',
            resolve: {
                id: ['$stateParams', function($stateParams) {
                    return $stateParams.id;
                    }]
                }
        })

        .state('dashboard.edit_my_temporary_post', {
            params : {id : null},
            url: '/edit_my_temporary_post',
            controller: 'editMyTmpPostCtrl',
            templateUrl: 'dashboard/controllers/post_temporary/edit_my_temp_post/edit_my_temporary_post.html',
            resolve: {
                id: ['$stateParams', function($stateParams) {
                    return $stateParams.id;
                }]
            }
        })

})