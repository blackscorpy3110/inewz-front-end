'use strict';

angular.module('myApp').run(['$rootScope', '$cookies',
    function ($rootScope, $cookies) {

        //if ($rootScope.currentLanguage == null) {
        //    $rootScope.currentLanguage = localStorageService.get('selectedLanguage') || 'en';
        //}
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            $rootScope.tokenAuth = $cookies.get('token-auth');
            $rootScope.role = $cookies.get('role');
            $rootScope.fullName = $cookies.get('fullName');

            console.log("Step: stateChangeStart");
            if ($rootScope.tokenAuth == undefined || $rootScope.tokenAuth == null) {
                // User isn't authenticated
                if (toState.name.indexOf("index.") == -1) {
                    $state.go("index.home");
                    event.preventDefault();
                    return;
                } else {
                    return;
                }
            }
            else {
                // User authenticated
                if (toState.name.indexOf("home.") == 0) {
                    $state.go("dashboard.home");
                    event.preventDefault();
                    return;
                }
            }
            if (fromState.isFather && toState.disableTransient) {
                return;
            }
        });

        //$rootScope.$on("$stateChangeError", function(event, current, previous, rejection){
        //
        //});
        //
        //$rootScope.$on('errorResponseMessage', function (event, errorResponse) {
        //    toaster.error("Có lỗi xảy ra. Thông tin lỗi", errorResponse.errorMessage);
        //});
        //
        //$rootScope.$on('successResponseMessage', function (event, successMessage) {
        //    toaster.success({title: successMessage});
        //});

    }]);