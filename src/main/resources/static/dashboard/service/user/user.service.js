/**
 * Created by Mr.Java on 1/27/2016.
 */
angular
    .module('services')
    .factory('userService', userService);

userService.$inject = ['$log', '$http', '$rootScope', '$cookies'];

function userService($log, $http, $rootScope) {
    $log.info('log from userService');

    return {
        loginApp: loginApp,
        userApp: userApp,
        registerApp: registerApp,
        updateUser: updateUser,
        delUser: delUser,
    };

    function loginApp(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/login',
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function userApp(successCallback, failureCallback) {
        return $http({
            url: 'http://localhost:8089/showAllUser?token=' + $rootScope.tokenAuth,
            method: 'GET',
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function registerApp(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/register',
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function updateUser(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/updateUser?token=' + $rootScope.tokenAuth,
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function delUser(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/delUser?token=' + $rootScope.tokenAuth,
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

};


