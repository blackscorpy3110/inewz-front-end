angular.module('services').factory('httpRequestInterceptor', ['$rootScope', '$q', function ($rootScope, $q) {
    return {
        request: function (requestConfig) {
            if ($rootScope.tokenAuth) {
                requestConfig.headers['token-auth'] = $rootScope.tokenAuth;
            }
            return requestConfig;
        },
        response: function (response) {
            // Do nothing on success response
            $rootScope.showOverlay = false;
            return response;
        },
        responseError: function (response) {
            var BreakException = {};
            try {
                var errorResponseInfo = {
                    errorMessage: "Error not clear",
                    handled: false,
                    status: response.status
                };

                var lastMatchError = null;
                $rootScope.errorContainer.forEach(function (error) {
                    if (response.status == error.status) {
                        if (response.data && response.data.errorMessage == error.key_message) {
                            errorResponseInfo.errorMessage = error.en_message;
                            errorResponseInfo.handled = error.handle ? error.handle : true; // Defined error is handled by default
                            return $q.reject(response);
                            //throw BreakException;
                        }
                    }
                });

                // If no error match
                $rootScope.$broadcast('errorResponseMessage', errorResponseInfo);
            } catch (e) {
                if (e !== BreakException) throw e;
            }

            return $q.reject(response);
        }
    };
}]);