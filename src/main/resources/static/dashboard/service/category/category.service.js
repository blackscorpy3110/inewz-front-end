/**
 * Created by Bui Cong Thanh on 1/28/2016.
 */
angular
    .module('services')
    .factory('categoryService', categoryService);

categoryService.$inject = ['$log', '$http', '$rootScope'];

function categoryService($log, $http, $rootScope) {
    $log.info('Category service');

    return {
        showAllCategory: showAllCategory,
        createCategory: createCategory,
        deleteCategory: deleteCategory,
        editCategory: editCategory,
        showOneCategory: showOneCategory
    };

    function showOneCategory(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            data: opts.data,
            url: 'http://localhost:8089/showOneCategory?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function showAllCategory(successCallback, failureCallback) {
        return $http({
            method: 'GET',
            url: 'http://localhost:8089/showAllCategory?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function createCategory(opts, successCallback, failureCallback) {
        var formData = new FormData();
        formData.append("field", JSON.stringify(opts.data));
        formData.append("file", opts.file);
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/createNewCategory?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': undefined},
            transformRequest: formData,
            data: formData
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function deleteCategory(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/deleteCategory?token=' + $rootScope.tokenAuth,
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function editCategory(opts, successCallback, failureCallback){
    var formData = new FormData();
    formData.append("field", JSON.stringify(opts.data));
    formData.append("file", opts.file);
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/editCategory?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': undefined},
            transformRequest: formData,
            data: formData
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }
}

