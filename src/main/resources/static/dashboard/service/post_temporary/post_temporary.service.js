/**
 * Created by Bui Cong Thanh on 1/29/2016.
 */
angular
    .module('services')
    .factory('postTemporaryService', postTemporaryService);

postTemporaryService.$inject = ['$log', '$http', '$rootScope'];

function postTemporaryService($log, $http, $rootScope) {
    $log.info('Post Temporary service');

    return {
        createNewPost: createNewPost,
        showAllPostTemporary: showAllPostTemporary,
        showAllMyPostTemporary: showAllMyPostTemporary,
        showOnePostTemporary: showOnePostTemporary,
        deleteTmpPost: deleteTmpPost,
        editTemporaryPost: editTemporaryPost,
        publishPost: publishPost,
    };

    function createNewPost(opts, successCallback, failureCallback) {
        var formData = new FormData();
        formData.append("field", JSON.stringify(opts.data));
        formData.append("file", opts.file);
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/createNewPost?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': undefined},
            transformRequest: formData,
            data: formData
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function showAllPostTemporary(successCallback, failureCallback) {
        return $http({
            method: 'GET',
            url: 'http://localhost:8089/showAllTemporaryPost?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function showAllMyPostTemporary(successCallback, failureCallback) {
        return $http({
            method: 'GET',
            url: 'http://localhost:8089/showAllMyTemporaryPost?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function showOnePostTemporary(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/showSingleTemporaryPost?token=' + $rootScope.tokenAuth,
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function deleteTmpPost(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/deleteTemporaryPost?token=' + $rootScope.tokenAuth,
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function editTemporaryPost(opts, successCallback, failureCallback) {
        var formData = new FormData();
        formData.append("field", JSON.stringify(opts.data));
        formData.append("file", opts.file);
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/editTemporaryPost?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': undefined},
            transformRequest: formData,
            data: formData
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function publishPost(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/publishPost?token=' + $rootScope.tokenAuth,
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }
}