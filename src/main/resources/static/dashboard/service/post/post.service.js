/**
 * Created by Bui Cong Thanh on 1/29/2016.
 */
angular
    .module('services')
    .factory('postService', postService);

postService.$inject = ['$log', '$http', '$rootScope'];

function postService($log, $http, $rootScope) {
    $log.info('post service');

    return {
        showAllPublishedPost: showAllPublishedPost,
        showAllMyPublishedPost: showAllMyPublishedPost,
        deletePublishedPost: deletePublishedPost,
        editPublishedPost: editPublishedPost,
        showOnePublishedPost: showOnePublishedPost,
        viewAllPost: viewAllPost,
        viewSinglePost: viewSinglePost,
        morePostSameCategory: morePostSameCategory,
        findPostByUserId: findPostByUserId,
        findPostByCategoryId: findPostByCategoryId
    };

    function findPostByUserId(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            data: opts.data,
            url: 'http://localhost:8089/findPostByUser',
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function findPostByCategoryId(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            data: opts.data,
            url: 'http://localhost:8089/findPostByCategory',
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function viewSinglePost(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            data: opts.data,
            url: 'http://localhost:8089/singlePost',
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function morePostSameCategory(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            data: opts.data,
            url: 'http://localhost:8089/morePostSameCategory',
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function viewAllPost(successCallback, failureCallback) {
        return $http({
            method: 'GET',
            url: 'http://localhost:8089/homepage',
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function showOnePublishedPost(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            data: opts.data,
            url: 'http://localhost:8089/showOnePost?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function showAllPublishedPost(successCallback, failureCallback) {
        return $http({
            method: 'GET',
            url: 'http://localhost:8089/showAllPublishedPost?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function showAllMyPublishedPost(successCallback, failureCallback) {
        return $http({
            url: 'http://localhost:8089/showAllMyPublishedPost?token=' + $rootScope.tokenAuth,
            method: 'POST',
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function deletePublishedPost(opts, successCallback, failureCallback) {
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/deletePublishPost?token=' + $rootScope.tokenAuth,
            data: opts.data,
            headers: {'Content-Type': 'application/json'}
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }

    function editPublishedPost(opts, successCallback, failureCallback) {
        var formData = new FormData();
        formData.append("field", JSON.stringify(opts.data));
        formData.append("file", opts.file);
        return $http({
            method: 'POST',
            url: 'http://localhost:8089/editPublishedPost?token=' + $rootScope.tokenAuth,
            headers: {'Content-Type': undefined},
            transformRequest: formData,
            data: formData
        })
            .success(function (response) {
                successCallback(response);
            })
            .error(function (response) {
                failureCallback(response)
            });
    }
}
