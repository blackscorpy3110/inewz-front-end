/**
 * Created by Bui Cong Thanh on 2/26/2016.
 */
angular.module('angularUtils.directives.dirPagination').controller('paginationBarCtrl', ['$scope', function ($scope) {
    function paginationBarController($scope) {
        $scope.pageChangeHandler = function(num) {
            console.log('going to page ' + num);
        };
    }
}]);