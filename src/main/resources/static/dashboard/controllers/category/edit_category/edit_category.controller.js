/**
 * Created by Bui Cong Thanh on 2/4/2016.
 */
angular.module('categoryModule').controller('editCategoryCtrl', ['$scope', 'id', '$location', 'categoryService', function ($scope, id, $location, categoryService) {

    categoryService.showOneCategory({data: id},
        function (data) {
            $scope.catName = data.categoryDTOs[0].catName;
            $scope.catId = data.categoryDTOs[0].catId;
            $scope.catDes = data.categoryDTOs[0].catDescription;
            $scope.catImage = data.categoryDTOs[0].categoryImage;

        }, function (error) {
            $scope.message = error.message;
        })

    $scope.editCat = editCategory;

    function editCategory(file){
        categoryService.editCategory({data: {
                catName: $scope.catName,
                catId: $scope.catId,
                catDescription: $scope.catDes,
                categoryImage: $scope.catImage,
            }, file: file},
            function(data){
                alert(data.message);
                $location.path('dashboard/show_all_category')
            }, function(error) {
                alert(error.message);
            }
        )
    }

    $scope.backToShow = backToShowPage;

    function backToShowPage(){
        $location.path("dashboard/show_all_category");
    }
}]);
