/**
 * Created by Bui Cong Thanh on 2/4/2016.
 */
angular.module('categoryModule').controller('showCatCtrl', ['$scope', 'categoryService', '$location', '$state', function ($scope, categoryService, $location, $state) {

    $scope.categories = [];
    categoryService.showAllCategory(
        function (data) {
            $scope.categories = data.categoryDTOs;
        }, function (error) {
            alert("Lỗi không thể hiển thị !");
        });

    $scope.delCategory = deleteCategory;

    function deleteCategory(cat) {
        categoryService.deleteCategory({data: cat},
            function (data) {
                alert(data.message);
            }, function (error) {
                alert(error.message);
            })
    }

    $scope.sendId = sendCatId;
    function sendCatId(catId){
        $state.go('dashboard.edit_category',{id : catId});
    }
}]);