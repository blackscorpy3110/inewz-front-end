/**
 * Created by Bui Cong Thanh on 1/28/2016.
 */
angular.module('categoryModule').controller('createCatCtrl', ['$scope', 'categoryService', '$location', function ($scope, categoryService, $location) {

    $scope.createCat = createCategory;

    function createCategory(file) {
        categoryService.createCategory({data: $scope.category,file: file},
            function (data) {
                alert(data.message);
                $location.path('dashboard/show_all_category')
            }, function (error) {
                alert(error.message);
            })
    }
}]);