/**
 * Created by Bui Cong Thanh on 1/29/2016.
 */
angular.module('postTemporaryModule').controller('showMyTempPostCtrl', ['$scope', 'postTemporaryService', '$state', '$location', '$mdDialog', '$mdMedia', function ($scope, postTemporaryService, $state, $location, $mdDialog, $mdMedia) {

    postTemporaryService.showAllMyPostTemporary(
        function (data) {
            $scope.currentPage = 1;
            $scope.pageSize = 10;
            $scope.myPostTmp = [];

            for (var i = 1; i <= 100; i++) {
                $scope.myPostTmp = data.searchTemporaryPostDTOs;
            }

        }, function (error) {
            alert(error.message);
        })

    $scope.delTempPost = deleteTmpPost;

    function deleteTmpPost(post) {
        postTemporaryService.deleteTmpPost({data: post},
            function (data) {
                alert(data.message);
            }, function (error) {
                alert(error.message);
            })
    }

    $scope.publish = publishPost;

    function publishPost(post){
        postTemporaryService.publishPost({data: post},
            function(data){
                alert(data.message);
                $location.path("dashboard/show_all_my_post");
            }, function(error){
                alert(error.message);
            })
    }

    $scope.sendMyPostId = sendTmpId;
    function sendTmpId(tmpPostId){
        $state.go('dashboard.edit_my_temporary_post',{id : tmpPostId});
    }

    $scope.status = '  ';
    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
    $scope.viewMySingleTmpPost = function (ev, id) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: function mySingleTmpPageCtrl($scope, $mdDialog, postTemporaryService) {
                postTemporaryService.showOnePostTemporary({data: id},
                    function (data) {
                        $scope.title = data.allTemporaryPostDTOs[0].tmpPostTitle;
                        $scope.postTempContent = data.allTemporaryPostDTOs[0].tmpPostContent;
                        $scope.categoryName = data.allTemporaryPostDTOs[0].catName;
                        $scope.image = data.allTemporaryPostDTOs[0].tmpPostImage;
                        $scope.user = data.allTemporaryPostDTOs[0].fullName;
                        $scope.date = data.allTemporaryPostDTOs[0].tmpPostDate;
                    }, function (error) {
                        alert(error.message);
                    })
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.back = function (answer) {
                    $mdDialog.hide(answer);
                };
            },
            templateUrl: 'dashboard/controllers/post_temporary/single_tmp_page/single_tmp_page.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen,
            locals: {
                data : id
            }
        })
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

}]);

