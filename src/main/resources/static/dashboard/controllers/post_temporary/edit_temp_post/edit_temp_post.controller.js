/**
 * Created by Bui Cong Thanh on 2/4/2016.
 */
angular.module('postTemporaryModule').controller('editTempPostCtrl', ['$scope', 'postTemporaryService', 'id', '$location', 'categoryService', function ($scope, postTemporaryService, id, $location, categoryService) {

    postTemporaryService.showOnePostTemporary({data: id},
        function (data) {
            $scope.title = data.allTemporaryPostDTOs[0].tmpPostTitle;
            $scope.postTempContent = data.allTemporaryPostDTOs[0].tmpPostContent;
            $scope.categoryId = data.allTemporaryPostDTOs[0].catId;
            $scope.categoryName = data.allTemporaryPostDTOs[0].catName;
            $scope.image = data.allTemporaryPostDTOs[0].tmpPostImage;
            $scope.tmpId = data.allTemporaryPostDTOs[0].tmpPostId;
            $scope.status = data.allTemporaryPostDTOs[0].postTmpStatus;

            $scope.categories = [];
            categoryService.showAllCategory(
                function (data) {
                    $scope.categories = data.categoryDTOs;
                }, function (error) {
                    error(error.message);
                });
        }, function (error) {
            alert(error.message);
        })

    $scope.editTmpPost = editTemporaryPost;

    function editTemporaryPost(file){
        postTemporaryService.editTemporaryPost({data: {
                tmpPostTitle: $scope.title,
                tmpPostContent: $scope.postTempContent,
                tmpPostId: $scope.tmpId,
                postTmpStatus: $scope.status,
                catId: $scope.categoryId
            },file : file},
            function(data){
                alert(data.message);
                $location.path("dashboard/show_all_my_temporary_post");
            }, function(error) {
                alert(error.message);
            }
        )
    }

    $scope.backToShow = backToShowPage;

    function backToShowPage(){
        $location.path("dashboard/show_all_temporary_post");
    }

}]);