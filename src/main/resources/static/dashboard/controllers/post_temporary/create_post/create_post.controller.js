/**
 * Created by Bui Cong Thanh on 2/4/2016.
 */
angular.module('postTemporaryModule').controller('createPostCtrl', ['$scope', 'postTemporaryService', '$location', 'categoryService', function ($scope, postTemporaryService, $location, categoryService) {
    $scope.categories = [];
    categoryService.showAllCategory(
        function (data) {
            $scope.categories = data.categoryDTOs;
        }, function (error) {
            alert(error.message);
        });

    $scope.createPost = createNewPost;

    function createNewPost(field,file) {
        postTemporaryService.createNewPost({data: field, file: file},
            function (data) {
                alert(data.message);
                $location.path("dashboard/show_all_my_temporary_post");
            }, function (error) {
                alert(error.message);
            })
    }
}]);