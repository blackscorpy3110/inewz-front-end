/**
 * Created by Mr.Java on 1/27/2016.
 */
angular.module('userModule').controller('loginCtrl', ['$scope', 'userService', '$location', '$cookies', function ($scope, userService, $location, $cookies) {

    $scope.submitLogin = submitLogin;

    function submitLogin() {
        userService.loginApp({data: $scope.login},
            function (data) {
                var myEl = angular.element(document.querySelector('#navbar'));
                var Elremove = angular.element(document.querySelector('#ulDefault'));

                $cookies.put('token-auth', data.token);
                $cookies.put('role', data.role);
                $cookies.put('fullName', data.fullName);
                $location.path('index/home');
            }, function (error) {

            })
    }
}]);

angular.module('userModule').controller('userCtrl', ['$scope', 'userService', '$location', function ($scope, userService, $location) {
    $scope.users = [];
    userService.userApp(
        function (data) {
            $location.path('dashboard/show_all_user')
            $scope.users = data.userManageDTOList;
        }, function (error) {

        });

    $scope.editUser = editUser;
    function editUser(user) {
        userService.updateUser({data: user},
            function (data) {
                $scope.user = data;
                $scope.message = data.message;
                $location.path('dashboard/show_all_user')
            }, function (error) {

            }
        )
    }

    $scope.delUser = delUser;
    function delUser(userId) {
        userService.delUser({data: userId},
            function (data) {
                $location.path('dashboard/show_all_user')
                $scope.message = data.message;
            }, function (error) {

            }
        )
    }
}]);

angular.module('userModule').controller('registerCtrl', ['$scope', 'userService', '$location', function ($scope, userService, $location) {
    $scope.submitRegister = submitRegister;

    function submitRegister() {
        userService.registerApp({data: $scope.user},
            function (data) {
                $location.path('index/login')
                $scope.message = data.message;
            }, function (error) {

            })
    }
}]);