/**
 * Created by Bui Cong Thanh on 2/29/2016.
 */
/**
 * Created by Bui Cong Thanh on 2/4/2016.
 */
angular.module('postModule').controller('findPostByUserCtrl', ['$scope', 'postService', 'id', '$mdDialog', '$mdMedia', '$state', 'categoryService', function ($scope, postService, id, $mdDialog, $mdMedia, $state, categoryService) {

    $scope.categories = [];
    categoryService.showAllCategory(
        function (data) {
            $scope.categories = data.categoryDTOs;
        }, function () {
            alert("Show category failed!");
        });

    postService.findPostByUserId({data: id},
        function (data) {
            $scope.currentPage = 1;
            $scope.pageSize = 30;
            $scope.posts = [];

            $scope.posts = data.searchPostDTOs;
            $scope.user = data.searchPostDTOs[0].fullName;
        }, function () {
            alert("Find post failed due to a system");
        })

    $scope.status = '  ';
    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
    $scope.viewSinglePost = function (ev, id) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: function singlePageCtrl($scope, $mdDialog, postService) {
                $scope.morePost = [];
                postService.viewSinglePost({data: id},
                    function (data) {
                        $scope.id = data.viewSinglePostDTOs[0].postId;
                        $scope.title = data.viewSinglePostDTOs[0].postTitle;
                        $scope.content = data.viewSinglePostDTOs[0].postContent;
                        $scope.categoryId = data.viewSinglePostDTOs[0].catId;
                        $scope.category = data.viewSinglePostDTOs[0].catName;
                        $scope.date = data.viewSinglePostDTOs[0].postDate;
                        $scope.image = data.viewSinglePostDTOs[0].postImage;
                        $scope.user = data.viewSinglePostDTOs[0].userId;
                        $scope.userName = data.viewSinglePostDTOs[0].fullName;

                        postService.morePostSameCategory({data:{
                            postId : $scope.id,
                            catId : $scope.categoryId
                        }}, function (data) {
                            $scope.morePost = data.viewMorePostDTOs;
                        }, function () {
                            alert("Show more post failed!");
                        })

                    }, function () {
                        alert("Show single post failed!");
                    })

                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.back = function (answer) {
                    $mdDialog.hide(answer);
                };
            },
            templateUrl: 'dashboard/controllers/post/single_page/single_dialog_page.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen,
            locals: {
                data : id,
            }
        })
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    $scope.sendCatId = sendCatId;
    function sendCatId(catId) {
        $state.go('index.find_post_by_category', {id: catId});
    }
}]);