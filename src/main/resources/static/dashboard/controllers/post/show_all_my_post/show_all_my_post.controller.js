/**
 * Created by Bui Cong Thanh on 2/16/2016.
 */
angular.module('postModule').controller('showMyPostCtrl', ['$scope', 'postService', '$state', '$mdDialog', '$mdMedia', function ($scope, postService, $state, $mdDialog, $mdMedia) {

    postService.showAllMyPublishedPost(
        function (data) {
            $scope.currentPage = 1;
            $scope.pageSize = 10;
            $scope.posts = [];

            for (var i = 1; i <= 100; i++) {
                $scope.posts = data.publishedPosts;
            }
        }, function (error) {
            alert("Show post temp failed!");
        })

    $scope.delPublishedPost = deletePublishedPost;

    function deletePublishedPost(post) {
        postService.deletePublishedPost({data: post},
            function () {
                alert("Delete Success!");
            }, function () {
                alert("Delete failed due to a system");
            })
    }

    $scope.sendId = sendPostId;
    function sendPostId(postId){
        $state.go('dashboard.edit_my_post',{id : postId});
    }

    $scope.status = '  ';
    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
    $scope.viewMyPost = function (ev, id) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: function viewMyPostCtrl($scope, $mdDialog, postService) {
                postService.viewSinglePost({data: id},
                    function (data) {
                        $scope.id = data.viewSinglePostDTOs[0].postId;
                        $scope.title = data.viewSinglePostDTOs[0].postTitle;
                        $scope.content = data.viewSinglePostDTOs[0].postContent;
                        $scope.date = data.viewSinglePostDTOs[0].postDate;
                        $scope.image = data.viewSinglePostDTOs[0].postImage;
                        $scope.user = data.viewSinglePostDTOs[0].userId;
                        $scope.userName = data.viewSinglePostDTOs[0].fullName;
                    }, function (error) {
                        alert("Show post temp failed!");
                    })
                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.back = function (answer) {
                    $mdDialog.hide(answer);
                };
            },
            templateUrl: 'dashboard/controllers/post/single_page/single_page_for_view.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen,
            locals: {
                data : id
            }
        })
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };
}]);
