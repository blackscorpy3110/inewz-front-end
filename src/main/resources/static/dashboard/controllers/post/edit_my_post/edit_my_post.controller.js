/**
 * Created by Bui Cong Thanh on 2/4/2016.
 */
angular.module('postModule').controller('editMyPostCtrl', ['$scope', 'postService', 'id', '$location', 'categoryService', function ($scope, postService, id, $location, categoryService) {

    postService.showOnePublishedPost({data: id},
        function (data) {
            $scope.title = data.showOnePostDTOs[0].postTitle;
            $scope.postTempContent = data.showOnePostDTOs[0].postContent;
            $scope.categoryId = data.showOnePostDTOs[0].catId;
            $scope.categoryName = data.showOnePostDTOs[0].catName;
            $scope.image = data.showOnePostDTOs[0].postImage;
            $scope.tmpId = data.showOnePostDTOs[0].tmpPostId;
            $scope.version = data.showOnePostDTOs[0].postVersion;
            $scope.user = data.showOnePostDTOs[0].userId;
            $scope.postId = data.showOnePostDTOs[0].postId;

            $scope.categories = [];
            categoryService.showAllCategory(
                function (data) {
                    $scope.categories = data.categoryDTOs;
                }, function (error) {
                    alert(error.message);
                });
        }, function (error) {
            alert(error.message);
        })

    $scope.editMyPost = editMyPublishedPost;

    function editMyPublishedPost(file){
        postService.editPublishedPost({data: {
                postTitle: $scope.title,
                postContent: $scope.postTempContent,
                tmpPostId: $scope.tmpId,
                catId: $scope.categoryId,
                postVersion: $scope.version,
                userId: $scope.user,
                postId: $scope.postId,
            }, file: file},
            function(data){
                alert(data.message);
                $location.path("dashboard/show_all_my_temporary_post");
            }, function(error) {
                alert(error.message);
            }
        )
    }

    $scope.backToShow = backToShowPage;

    function backToShowPage(){
        $location.path("dashboard/show_all_my_post");
    }
}]);