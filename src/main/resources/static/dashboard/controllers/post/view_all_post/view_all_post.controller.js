angular.module('postModule').controller('viewAllPostCtrl', ['$scope', 'postService', '$mdDialog', '$mdMedia', '$state', 'categoryService', function ($scope, postService, $mdDialog, $mdMedia, $state, categoryService) {

    $scope.posts = [];
    postService.viewAllPost(
        function (data) {
            $scope.posts = data.viewAllPublishedPostDTOList;
        }, function () {
            alert("Show post failed!");
        })

    $scope.categories = [];
    categoryService.showAllCategory(
        function (data) {
            $scope.categories = data.categoryDTOs;
        }, function () {
            alert("Show category failed!");
        });

    $scope.status = '  ';
    $scope.customFullscreen = $mdMedia('xs') || $mdMedia('sm');
    $scope.viewSinglePost = function (ev, id) {
        var useFullScreen = ($mdMedia('sm') || $mdMedia('xs')) && $scope.customFullscreen;
        $mdDialog.show({
            controller: function singlePageCtrl($scope, $mdDialog, postService) {
                $scope.morePost = [];
                postService.viewSinglePost({data: id},
                    function (data) {
                        $scope.id = data.viewSinglePostDTOs[0].postId;
                        $scope.title = data.viewSinglePostDTOs[0].postTitle;
                        $scope.content = data.viewSinglePostDTOs[0].postContent;
                        $scope.categoryId = data.viewSinglePostDTOs[0].catId;
                        $scope.category = data.viewSinglePostDTOs[0].catName;
                        $scope.date = data.viewSinglePostDTOs[0].postDate;
                        $scope.image = data.viewSinglePostDTOs[0].postImage;
                        $scope.user = data.viewSinglePostDTOs[0].userId;
                        $scope.userName = data.viewSinglePostDTOs[0].fullName;

                        postService.morePostSameCategory({data:{
                                postId : $scope.id,
                                catId : $scope.categoryId
                            }}, function (data) {
                                $scope.morePost = data.viewMorePostDTOs;
                            }, function () {
                                alert("Show more post failed!");
                            })

                    }, function () {
                        alert("Show single post failed!");
                    })

                $scope.hide = function () {
                    $mdDialog.hide();
                };
                $scope.cancel = function () {
                    $mdDialog.cancel();
                };
                $scope.back = function (answer) {
                    $mdDialog.hide(answer);
                };
            },
            templateUrl: 'dashboard/controllers/post/single_page/single_dialog_page.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: useFullScreen,
            locals: {
                data : id,
            }
        })
        $scope.$watch(function () {
            return $mdMedia('xs') || $mdMedia('sm');
        }, function (wantsFullScreen) {
            $scope.customFullscreen = (wantsFullScreen === true);
        });
    };

    $scope.sendUserId = sendUserId;
    function sendUserId(userId) {
        $state.go('index.find_post_by_user', {id: userId});
    }

    $scope.sendCatId = sendCatId;
    function sendCatId(catId) {
        $state.go('index.find_post_by_category', {id: catId});
    }
}]);