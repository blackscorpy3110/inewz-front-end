angular.module('myApp').config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
        $urlRouterProvider.when('', '/home/default');

        $httpProvider.defaults.headers.get = {'Content-Type': 'application/json'};
        $httpProvider.defaults.headers.common = {'Content-Type': 'application/json'};
        $httpProvider.defaults.headers.post = {'Content-Type': 'application/json'};
        $httpProvider.defaults.headers.put = {'Content-Type': 'application/json'};
        $httpProvider.defaults.headers.patch = {'Content-Type': 'application/json'};

        $httpProvider.interceptors.push('httpRequestInterceptor');
    }]);
;