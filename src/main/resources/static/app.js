/**
 * Created by Bui Cong Thanh on 2/4/2016.
 */

angular.module('navApp', []);
//user modules
angular.module('userModule',[]);

//services module
angular.module('services', []);

//category modules
angular.module('categoryModule', []);

//post temporary modules
angular.module('postTemporaryModule', []);

//post modules
angular.module('postModule', []);

angular.module('paginationBarCtrl', []);
angular.module('filters', []).filter('truncate', function () {
    return function (text, length, end) {
        if (isNaN(length))
            length = 150;

        if (end === undefined)
            end = "...";

        if (text.length <= length || text.length - end.length <= length) {
            return text;
        }
        else {
            return String(text).substring(0, length - end.length) + end;
        }

    };
});
angular.module('myApp', ['ngMessages', 'filters', 'ngMaterial', 'ngSanitize', 'ngCkeditor','ngFileUpload',
    'angularUtils.directives.dirPagination',
    'ui.router', 'ngAnimate', 'ui.bootstrap',
    'ngCookies', 'slideMenu', 'services',
    'userModule',
    'categoryModule',
    'postTemporaryModule',
    'postModule'
]);
